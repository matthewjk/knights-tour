import pygame

class Knight:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.cellsize = 600 / size
        imgsize = int((self.cellsize) - (self.cellsize / 5))
        self.img = pygame.transform.scale(
            pygame.image.load("assets/knight.png"), (imgsize, imgsize)
        )
        self.moves = [[self.x, self.y]]
        self.lines = [
            [
                (self.x * self.cellsize) + self.cellsize / 2,
                (self.y * self.cellsize) + self.cellsize / 2,
            ]
        ]

    def draw(self, screen):
        pygame.draw.lines(screen, (0, 0, 0), False, self.lines, 5)
        xpos = int((self.moves[0][0] * self.cellsize) + (self.cellsize / 2))
        ypos = int((self.moves[0][1] * self.cellsize) + (self.cellsize / 2))
        pygame.draw.circle(screen, (0, 0, 0), (xpos, ypos), int(self.cellsize / 4))
        xpos = int((self.x * self.cellsize) + self.cellsize * 0.1)
        ypos = int((self.y * self.cellsize) + self.cellsize * 0.1)
        screen.blit(self.img, [xpos, ypos])
        # screen.blit(self.img,self.img.get_rect().center)

    def annotate(self, screen):
        font = pygame.font.Font(pygame.font.get_default_font(), 12)
        for i in range(0, len(self.moves), 1):
            text = font.render(str(i), True, (0, 0, 0))
            xpos = self.cellsize * self.moves[i][0]
            ypos = self.cellsize * self.moves[i][1]
            screen.blit(text, (xpos, ypos))

    def move(self, x, y):
        self.x = x
        self.y = y
        self.moves.append([x, y])
        self.lines.append(
            [
                (x * self.cellsize) + self.cellsize / 2,
                (y * self.cellsize) + self.cellsize / 2,
            ]
        )
