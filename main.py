import sys, pygame
from board import Board

if __name__ == "__main__":

    board = Board(18)

    pygame.init()

    pygame.display.set_caption("Knights Tour")
    pygame.display.set_icon(pygame.image.load("assets/icon.png"))
    screen = pygame.display.set_mode([600, 600])

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    board.toggledrawmode()

        board.draw(screen)
        pygame.display.flip()
