# Knight's tour
An implementation of the [knight's tour](https://en.wikipedia.org/wiki/Knight%27s_tour) problem made as an excuse to play around with [pygame](https://www.pygame.org/)

# Requirements:
- [Python3](https://www.python.org/)
- [Pygame](https://www.pygame.org/)

# Screenshots:

Visualisations can be toggled by pressing the enter key.

## Line mode:
![lines](screenshots/lines.png "Lines")

## Annotated mode:
![annotated](screenshots/annotated.png "Annotated")
