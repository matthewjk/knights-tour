import random
import pygame

from knight import Knight
from cell import Cell

class Board:
    def __init__(
        self, size, startx=random.randrange(0, 7, 1), starty=random.randrange(0, 7, 1)
    ):
        # 0 index grid from top left
        self.size = size
        self.lines = True
        self.knight = Knight(startx, starty, size)
        self.cells = []
        self.generatecells()
        self.generateneighbours()
        self.cells[startx][starty].visited = True
        self.tour()
    
    def toggledrawmode(self):
        self.lines = not self.lines
        if self.lines:
            print("Drawing lines")
        else:
            print("Annotating")

    def draw(self, screen):
        for i in range(0, self.size, 1):
            for cell in self.cells[i]:
                cell.draw(screen)
        if self.lines == True:
            self.knight.draw(screen)
        else:
            self.knight.annotate(screen)

    def generatecells(self):
        for x in range(0, self.size, 1):
            self.cells.append([])
            for y in range(0, self.size, 1):
                self.cells[x].append(Cell(x, y, self.size))

    def generateneighbours(self):
        moves = [
            [2, 1],
            [2, -1],
            [-2, +1],
            [-2, -1],
            [1, 2],
            [-1, 2],
            [1, -2],
            [-1, -2],
        ]

        for x in range(0, self.size, 1):
            for y in range(0, self.size, 1):
                for m in moves:
                    if (
                        (self.cells[x][y].x + m[0] < self.size)
                        and (self.cells[x][y].x + m[0] >= 0)
                        and (self.cells[x][y].y + m[1] < self.size)
                        and (self.cells[x][y].y + m[1] >= 0)
                    ):
                        self.cells[x][y].candidates.append(self.cells[x + m[0]][y + m[1]])

    def heuristic(self):
        for i in self.cells[self.knight.x][self.knight.y].candidates:
            for j in i.candidates:
                if j.visited == True:
                    i.candidates.remove(j)
            if i.visited == True:
                self.cells[self.knight.x][self.knight.y].candidates.remove(i)

        ret = False
        for i in self.cells[self.knight.x][self.knight.y].candidates:
            if i.visited == False:
                ret = i
                break
        for i in self.cells[self.knight.x][self.knight.y].candidates:
            if i.visited == False and (len(i.candidates) <= len(ret.candidates)):
                ret = i
        return ret

    def tour(self):
        while True:
            visit = self.heuristic()
            if visit == 0:
                break
            # print("Moving to: " + str(visit.x) + ", " + str(visit.y))
            self.knight.move(visit.x, visit.y)
            visit.visited = True
        print(self.knight.moves)
