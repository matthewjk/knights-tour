import pygame

class Cell:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = 600 / size
        self.visited = False
        self.candidates = []
        self.colour = (213, 171, 111) if ((x + y) % 2 == 0) else (139, 69, 18)

    def printinfo(self):
        print("Position: (" + str(self.x) + ", " + str(self.y) + ")")

    def draw(self, screen):
        pygame.draw.rect(
            screen,
            self.colour,
            (self.x * self.size, self.y * self.size, self.size, self.size),
            0,
        )
